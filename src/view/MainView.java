package view;

import java.util.HashMap;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Scanner;

import model.IOXML.extendedData;
import model.IOXML.simpleData;

public class MainView {

	
	Scanner scan = new Scanner(System.in);
	protected int choice;
	
	public MainView() {
		
	}
	
	// Display logo
	public void displayLogo() {		                                          
		System.out.println("\\ / _ |_ _|__  _  _ ");
		System.out.println(" | (_|| | | /_(/_(/_");
	}
	
	// Main Menu display
	public int displayMainMenu() {
		this.displayLogo();
		System.out.println("\nWelcome to Yahtzee-Game");
		System.out.println("Choose: ");
		System.out.println("1. Start new game");
		System.out.println("2. Load game");
		System.out.println("3. Exit the application");
		choice = this.selectOption();
		return choice;
	}
	
	// New game -> mode selection
	public int displayGameModes() {
		System.out.println("\nChoose game mode");
		System.out.println("Option: ");
		System.out.println("1. Standard");
		System.out.println("2. Maxi");
		System.out.println("3. Return to the main menu");
		choice = this.selectOption();
		return choice;
	}
	
	// New game -> mode selection -> select amount of players (human)
	public int enterHuman() {
		System.out.print("\nEnter human player count (1 - 5)");
		choice = this.selectOption();
		return choice;
	}
	
	// New game -> mode selection -> select amount of players (bot)
	public int enterBOT(int space_left) {
		System.out.print("\nEnter bot player count (slots left: " + space_left + ")");
		choice = this.selectOption();
		return choice;
	}
	
	// New game -> mode selection -> amount decided -> Enter name
	public String enterName(int count) {
		System.out.print("\nEnter " + (count + 1)  + " player name: ");
		String choice = this.selectName();
		return choice;
	}
	
	
	// Load game
	public int chooseGameVerboseMode() {
		System.out.println("Choose listing mode: ");
		System.out.println("1. Simple [game_path | date | player(s) : total score]");
		System.out.println("2. Extended [game_path | player(s) : score list]");
		System.out.println("3. Return to main menu");
		
		return this.selectOption();
	}
	
	// Load game -> simple verbose
	public int chooseGameSV(Map<String, simpleData[]> listing) {
		System.out.println("Simple verbose");
		System.out.println("Choose appropriate index to load the game! Enter (-1) to exit.");

		int index = 0;
		
		for (Entry<String, simpleData[]> row : listing.entrySet()) {
			System.out.println(index + ". " + row.getKey());
			
			simpleData[] temp_data = row.getValue();
			
			for (simpleData x : temp_data) {
				System.out.println(x.date + " " + x.name + " " + x.grand_total);
			}
			
			System.out.print("\n");
			
			index++;
		}
		
		return this.selectOption();
	}
	
	// Load game -> extended verbose
	public int chooseGameEV(Map<String, extendedData[]> listing) {
		System.out.println("Extended verbose");
		System.out.println("Choose appropriate index to load the game! Enter (-1) to exit.");
		
		int index = 0;

		for (Entry<String, extendedData[]> row : listing.entrySet()) {
			System.out.println();
			System.out.println(index + ". " + row.getKey());
			
			extendedData[] temp_data = row.getValue();
			
			for (extendedData x : temp_data) {
				System.out.println();
				System.out.println("Name: " + x.name);
				HashMap<String, Integer> temp_score = x.scorelist;
				
				int inner_index = 1;
				
				for (Entry<String, Integer> local_row : temp_score.entrySet()) {
					System.out.printf("%-18s : %-8s", local_row.getKey(), local_row.getValue());
					
					if (inner_index % 3 == 0) {
						System.out.println();
					}
					
					inner_index++;
				}
				System.out.println();
			}
			
			index++;
		}
		
		return this.selectOption();
	}
	
	// Generic function to acquire user's input (int-based)
	public int selectOption() {
		System.out.print("\nYour choice: ");
		choice = scan.nextInt();
		return choice;
	}
	
	// Generic function to acquire user's input (string-based)
	public String selectName() {
		System.out.print("\nYour choice: ");
		String choice = scan.next();
		return choice;
	}
	
	// Display error
	public void displayError(String error_message) {
		System.out.println("\n");
		System.out.println("Error occurred!");
		System.out.println("Explanation: " + error_message);
	}
}
