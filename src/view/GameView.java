package view;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;
import java.util.Scanner;

import model.Dice;

// View for the game -> controls display and messages
public class GameView {
	
	Scanner scan = new Scanner(System.in);
	
	public GameView() {}
	
	// Display general information about round
	public void displayGeneralInfo(int round, int max_rerolls, String name, Dice[] dices, HashMap<String, Integer> combination_delta_holder) {
		System.out.println("\nGeneral information");
		System.out.println("Current roll: " + round);
		System.out.println("Max rerolls: " + max_rerolls);
		System.out.println("Contestant's name: " + name);
		System.out.print("Dices: ");
	
		for (Dice dice : dices) {
			System.out.print(dice.getValue() + " ");
		}
		
		System.out.println("\n========================================");
		System.out.println("Combinations to select: ");
		
		for (Map.Entry<String, Integer> combo : combination_delta_holder.entrySet()) {
			System.out.println(combo.getKey() + " : " + combo.getValue());
		}
		
	}
	
	// Display current turn
	public void displayGameTurn(int turn, int max_turn) {
		System.out.println("Current turn: "+ turn + " | Maximum turns: " + max_turn);
	}
	
	// Display current score list 
	public void displayScoreList(HashMap<String, Integer> score_list) {
		
		System.out.println("========================================");
		System.out.println("Current score list");
		System.out.println("========================================");
		
		// Display only keys with values that are not equal to null (unused combination)
		for (Map.Entry<String, Integer> combo : score_list.entrySet()) {
			
			if (combo.getValue() != null) {
				System.out.println(combo.getKey() + " : " + combo.getValue());
			}
			
		}
		
		System.out.println("========================================");
		
	}
	
	// Display re-roll menu
	public int displayReRollMenu() {
		System.out.println("Choose re-roll option:");
		System.out.println("1. Re-roll all dices");
		System.out.println("2. Select dices for re-roll");
		System.out.println("3. Return to game menu");
		
		return this.readUserInput();
	}
	
	// Display selective dice rolls
	public int displaySelectiveRoll(Dice[] dices, ArrayList<Integer> dices_index) {
		System.out.println("Choose index of dice which to roll (current indexes: " + dices_index + " ):");
		System.out.println("Enter -1 to stop the iteration!");
		
		int index = 0;
		
		for (Dice dice : dices) {
			System.out.print(index + ". dice, value: " + dice.getValue() + "\n");
			index++;
		}
		
		return this.readUserInput();
	}
	
	// Display selective roll handler message
	public void displayRollHandling(int index) {
		System.out.println("The dice with index (" + index + ") was removed as a candidate for re-roll!");
	}
	
	// Display re-roll message
	public void displayReRoll(Dice[] dices) {
		System.out.println("Dices were re-rolled. New values:");
		
		for (Dice dice : dices) {
			System.out.print(" " + dice.getValue());
		}
		
		System.out.println("\n");
	}
	
	// Display error
	public void displayGameError(String message) {
		System.out.println("\n");
		System.out.println("Game error occurred!");
		System.out.println("Explanation: " + message);
	}
	
	// Display end-game message
	public void displayEndGameMessage(String status) {
		System.out.println("\n");
		System.out.println("The end of the game reached!");
		System.out.println("Status of the game: " + status);
	}
		
	// Read the user input (int)
	public int readUserInput() {

		System.out.print("\nYour selection: ");
		int choice = scan.nextInt();
		
		return choice;
	}
	
	// Choose combination for score list
	public int chooseCombination(HashMap<String, Integer> combination_delta_holder) {
		
		int index = 0;
		
		System.out.println("\nChoose combination by index (enter -1 to exit in game menu): ");
		
		for (Map.Entry<String, Integer> combo : combination_delta_holder.entrySet()) {
			System.out.println(index + ". "+ combo.getKey() + " " + combo.getValue());
			index++;
		}
		
		return this.readUserInput();
	}
	
	// Display action menu
	public void displayActionMenu(int current_reroll, int max_reroll) {
		// Local inits
		
		System.out.println("\nChoose action: ");
		System.out.println("1. Choose available combination");
		System.out.println("2. Display current score list");
		
		if (current_reroll < max_reroll) {
			System.out.println("3. Re-roll dices");
			System.out.println("4. Save & exit");
			System.out.println("5. Exit without save");
		} else {
			System.out.println("3. Save & exit");
			System.out.println("4. Exit without save");
		}

		
	}

}
