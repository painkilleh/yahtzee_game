package program;

import controller.MainController;

public class Program {

	public static void main(String[] args) {
		
		MainController controller = new MainController();
		controller.execute();

	}

}
