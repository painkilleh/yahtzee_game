package model;

import java.util.ArrayList;
import java.util.HashMap;

public class Player extends Contestant {

	public Player(String name) {
		super(name);
	}
	
	// User interaction
	public int provideUserInput(Dice[] dices, boolean yahtzee_flag, ScoreClassifier SC) {
		
		while (this.getCurrentReroll() < this.getMaxRerolls() + 1) {
			
			HashMap<String, Integer> current_delta_combo = SC.returnDeltaCombinations(this.getScoreList(), SC.showPossibleCombinations(dices), yahtzee_flag);
			
			GV.displayGeneralInfo(this.getCurrentReroll(), this.getMaxRerolls(), this.getName(), dices, current_delta_combo);	
			GV.displayActionMenu(this.getCurrentReroll(), this.getMaxRerolls());
			
			int choice = GV.readUserInput();
			
			// Condition for menu possible options
			boolean reroll_condition = this.getCurrentReroll() < this.getMaxRerolls();
			int option_delimiter = 0;
			
			// If the round count is less than max rerolls (used for appropriate 
			if (reroll_condition == true) {
				option_delimiter = 5;
			} else {
				option_delimiter = 4;
			}
			
			// Action menu choice
			while (choice <= 0 || choice > option_delimiter) {
				GV.displayGameError("Selection of game menu is wrong! Retrying...");
				choice = GV.readUserInput();
			}
			
			// Selection of appropriate action
			if (option_delimiter == 5) {
				
				switch (choice) {
				// Choose combo
				case 1:
					int index_combo = GV.chooseCombination(current_delta_combo);
					
					while (index_combo > current_delta_combo.size()) {
						GV.displayGameError("Wrong selection of index. Retrying...");
						index_combo = GV.chooseCombination(current_delta_combo);
					}
					
					if (index_combo == -1) {
						GV.displayGeneralInfo(this.getCurrentReroll(), this.getMaxRerolls(), this.getName(), dices, current_delta_combo);	
						this.provideUserInput(dices, yahtzee_flag, SC);
						// break;
					}
					
					this.applyCombination(index_combo, current_delta_combo);	
					
					return 0;
					
				// Display score list
				case 2:
					GV.displayScoreList(this.getScoreList());
					break;
					
				// Re-roll dices
				case 3:
					choice = GV.displayReRollMenu();
					
					// Error block
					while (choice != 1 && choice != 2 && choice != 3) {
						GV.displayGameError("Wrong selection of menu! Retrying...");
						choice = GV.displayReRollMenu();
					}
					
					// Re-roll all dices
					if (choice == 1) {
						this.rollDices(dices);
						GV.displayReRoll(dices);
						
						this.setCurrentReroll(this.getCurrentReroll() + 1);
						break;
						
					// Selective reroll
					} else if (choice == 2) {
						
						ArrayList<Integer> dices_index = new ArrayList<Integer>();
						
						int result = GV.displaySelectiveRoll(dices, dices_index);
											
						// Loop for addition/removal of indexes for dices
						while (true) {
							
							if (result == -1) {
								break;
							} 
							
							else if (result < dices.length) {
								
								if (dices_index.contains(result)) {
									GV.displayRollHandling(result);
									dices_index.remove(dices_index.indexOf(result));
									
								} else {
									dices_index.add(result);
								}
								
								result = GV.displaySelectiveRoll(dices, dices_index);
							}
							else {
								GV.displayGameError("Wrong selection of dice index! Retrying...");
								result = GV.displaySelectiveRoll(dices, dices_index);
							}
							
						}
						
						// Rolling the dices
						if (dices_index.isEmpty() == false) {
							this.rollDices(dices_index, dices);
							GV.displayReRoll(dices);
							this.setCurrentReroll(this.getCurrentReroll() + 1);
						}
						
						break;

					} 
					// Exit to menu
					else if (choice == 3) {
						break;
					}
					
					break;
				// Save and exit
				case 4:
					// exit-code to trigger save game function
					return 1;
				// Exit without save
				case 5:
					// exit-code to trigger leave the game wo saving
					return 2;
				default:
					break;
				}
				
			} else if (option_delimiter == 4) {
				
				switch (choice) {
				// Choose combo
				case 1:
					int index_combo = GV.chooseCombination(current_delta_combo);
					
					while (index_combo > current_delta_combo.size()) {
						GV.displayGameError("Wrong selection of index. Retrying...");
						index_combo = GV.chooseCombination(current_delta_combo);
					}
					
					if (index_combo == -1) {
						GV.displayGeneralInfo(this.getCurrentReroll(), this.getMaxRerolls(), this.getName(), dices, current_delta_combo);	
						this.provideUserInput(dices, yahtzee_flag, SC);
					}
					
					this.applyCombination(index_combo, current_delta_combo);
					
					return 0;
				// Display score list
				case 2:	
					GV.displayScoreList(this.getScoreList());
					break;
				// Save and exit
				case 3:
					// exit-code to trigger save game function
					return 1;
				// Exit without save
				case 4:
					// exit-code to trigger leave the game wo saving
					return 2;
				default:
					break;
				}
				
			}
			
		}
		
		return 0;
	}
	
}
