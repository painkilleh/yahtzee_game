package model;

public class ContestantDefaultVariables {
	
	// Variables for default amount of turns for each player
	// Default re-roll value
	private final static int default_reroll_value = 1;
	private final static int default_max_reroll_value = 3;
	
	public int getDefaultRerollValue() {
		return default_reroll_value;
	}
	
	public int getDefaultMaxRerollValue() {
		return default_max_reroll_value;
	}

}
