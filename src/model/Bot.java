package model;

import java.util.HashMap;
import java.util.Random;
import java.util.Map.Entry;

public class Bot extends Contestant {
	
	private int ID = (int) (Math.random()*9000) + 1000;
	protected Random randomGenerator = new Random();

	public Bot() {
		this.name = "BOT_#" + Integer.toString(ID);
	}
	
	// For bot selection
	// Greedy-type
	private int findBestCombination(HashMap<String, Integer> bot_delta_combo) {
		
		int maxValue = 0;
		int maxIndex = 0;
		int index  = 0;
		
		for(Entry<String, Integer> row : bot_delta_combo.entrySet()) {
			
			if (row.getValue() > maxValue) {
				maxValue = row.getValue();
				maxIndex = index;
			}
			
			index++;
			
		}
		
		if (maxValue == 0)
			return -1;
		
		return maxIndex;
	}
	
	private void commandBot(HashMap<String, Integer> bot_delta_combo, boolean yahtzee_flag) {
		
		int choice = this.findBestCombination(bot_delta_combo);

		if (choice == -1) {
			
			int random_choice;
			
			if (bot_delta_combo.size() == 1) {
				random_choice = 0;
			}
			else {
				random_choice = randomGenerator.nextInt(bot_delta_combo.size());
			}
			
			this.applyCombination(random_choice, bot_delta_combo);
		}
		else {
			this.applyCombination(choice, bot_delta_combo);
		}

	}
	
	public int executeLogic(Dice[] dices, boolean yahtzee_flag, ScoreClassifier SC) {
		
		HashMap<String, Integer> delta_combo = SC.returnDeltaCombinations(this.getScoreList(), SC.showPossibleCombinations(dices), yahtzee_flag);
		
		this.commandBot(delta_combo, yahtzee_flag);
		
		return 0;
	}

}
