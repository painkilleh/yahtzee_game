package model;

import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.Map.Entry;

public class MaxiListClassifier extends ScoreClassifier {
	
	// For sorting pairs
	protected ArrayList<Integer> pair_holder = null;
	
	public MaxiListClassifier() {}

	@Override
	public HashMap<String, Integer> showPossibleCombinations(Dice[] dices) {
		this.classification_holder = new HashMap<String, Integer>();
		this.occurance_holder = new HashMap<Integer, Integer>();
		
		Dice[] local_dice = dices;
		this.sortDices(local_dice);
		this.initializeOccurrenceList();
		this.countOccurrences(local_dice);
		
		
		// Single combos
		checkOnes(local_dice);
		checkTwos(local_dice);
		checkThrees(local_dice);
		checkFours(local_dice);
		checkFives(local_dice);
		checkSixes(local_dice);
		
		// Complex combos
		checkPair(local_dice);
		check2Pair(local_dice);
		check3Pair(local_dice);
		check3OK(local_dice);
		check4OK(local_dice);
		check5OK(local_dice);
		checkSS(local_dice);
		checkLS(local_dice);
		checkFS(local_dice);
		checkFH(local_dice);
		checkVilla(local_dice);
		checkTower(local_dice);
		checkChance(local_dice);
		checkMaxi(local_dice);
		
		return this.classification_holder;
	}

	// http://www.maxiyahtzee.com/ -> used for combination building -> https://www.famholen.com/index.html
	// Single/upper combination check
	private void checkOnes(Dice[] local_dice) {
		
		int local_score = 0;
		
		for (Entry<Integer, Integer> row : occurance_holder.entrySet()) {
			
			if (row.getKey() == 1) {
				local_score = row.getKey() * row.getValue();
				break;
			}
			
		}
		
		if (local_score > 0) {
			classification_holder.put("one", local_score);
		}
		
	}

	private void checkTwos(Dice[] local_dice) {
		
		int local_score = 0;
		
		for (Entry<Integer, Integer> row : occurance_holder.entrySet()) {
			
			if (row.getKey() == 2) {
				local_score = row.getKey() * row.getValue();
				break;
			}
			
		}
		
		if (local_score > 0) {
			classification_holder.put("two", local_score);
		}
		
	}

	private void checkThrees(Dice[] local_dice) {
		
		int local_score = 0;
		
		for (Entry<Integer, Integer> row : occurance_holder.entrySet()) {
			
			if (row.getKey() == 3) {
				local_score = row.getKey() * row.getValue();
				break;
			}
			
		}
		
		if (local_score > 0) {
			classification_holder.put("three", local_score);
		}
		
	}

	private void checkFours(Dice[] local_dice) {
		
		int local_score = 0;
		
		for (Entry<Integer, Integer> row : occurance_holder.entrySet()) {
			
			if (row.getKey() == 4) {
				local_score = row.getKey() * row.getValue();
				break;
			}
			
		}
		
		if (local_score > 0) {
			classification_holder.put("four", local_score);
		}
	}

	private void checkFives(Dice[] local_dice) {
		
		int local_score = 0;
		
		for (Entry<Integer, Integer> row : occurance_holder.entrySet()) {
			
			if (row.getKey() == 5) {
				local_score = row.getKey() * row.getValue();
				break;
			}
			
		}
		
		if (local_score > 0) {
			classification_holder.put("five", local_score);
		}
		
	}

	private void checkSixes(Dice[] local_dice) {
		
		int local_score = 0;
		
		for (Entry<Integer, Integer> row : occurance_holder.entrySet()) {
			
			if (row.getKey() == 6) {
				local_score = row.getKey() * row.getValue();
				break;
			}
			
		}
		
		if (local_score > 0) {
			classification_holder.put("six", local_score);
		}
		
	}

	// Multiple/lower combination check
	private void checkPair(Dice[] local_dice) {
				
		pair_holder = new ArrayList<Integer>(); 
		
		for (Entry<Integer, Integer> row : occurance_holder.entrySet()) {
			
			if (row.getValue() == 2) {
				pair_holder.add(row.getKey() * row.getValue());
			}
			
		}
		
		// Sorting pairs 
		Collections.sort(pair_holder, Collections.reverseOrder());
		
		if (pair_holder.isEmpty() == false) {
			
			int value = pair_holder.get(0);
			
			classification_holder.put("one_pair", value);
		}
		
	}
	
	private void check2Pair(Dice[] local_dice) {
		
		pair_holder = new ArrayList<Integer>(); 
		
		for (Entry<Integer, Integer> row : occurance_holder.entrySet()) {
			
			if (row.getValue() == 2) {
				pair_holder.add(row.getKey() * row.getValue());
			}
			
		}
		
		// Sorting pairs 
		Collections.sort(pair_holder, Collections.reverseOrder());
		
		if (pair_holder.isEmpty() == false && pair_holder.size() >= 2) {
			
			int first_largest_pair = pair_holder.get(0);
			int second_largest_pair = pair_holder.get(1);
			
			classification_holder.put("two_pair", (first_largest_pair + second_largest_pair));
		}
		
	}
	
	private void check3Pair(Dice[] local_dice) {
		
		pair_holder = new ArrayList<Integer>(); 
		
		for (Entry<Integer, Integer> row : occurance_holder.entrySet()) {
			
			if (row.getValue() == 2) {
				pair_holder.add(row.getKey() * row.getValue());
			}
			
		}
		
		// Sorting pairs 
		Collections.sort(pair_holder, Collections.reverseOrder());
		
		if (pair_holder.isEmpty() == false && pair_holder.size() >= 3) {
			
			int first_largest_pair = pair_holder.get(0);
			int second_largest_pair = pair_holder.get(1);
			int third_largest_pair = pair_holder.get(2);
			
			classification_holder.put("three_pair", (first_largest_pair + second_largest_pair + third_largest_pair));
		}
		
	}
	
	// 3 of a kind
	private void check3OK(Dice[] local_dice) {
		
		pair_holder = new ArrayList<Integer>(); 
		
		for (Entry<Integer, Integer> row : occurance_holder.entrySet()) {
			
			if (row.getValue() == 3) {
				pair_holder.add(row.getKey() * row.getValue());
			}
			
		}
		
		// Sorting pairs 
		Collections.sort(pair_holder, Collections.reverseOrder());
		
		if (pair_holder.isEmpty() == false) {
			
			int value = pair_holder.get(0);
			
			classification_holder.put("three_of_kind", value);
		}
	}
	
	// 4 of a kind
	private void check4OK(Dice[] local_dice) {
		
		pair_holder = new ArrayList<Integer>(); 
		
		for (Entry<Integer, Integer> row : occurance_holder.entrySet()) {
			
			if (row.getValue() == 4) {
				pair_holder.add(row.getKey() * row.getValue());
			}
			
		}
		
		// Sorting pairs 
		Collections.sort(pair_holder, Collections.reverseOrder());
		
		if (pair_holder.isEmpty() == false) {
			
			int value = pair_holder.get(0);
			
			classification_holder.put("four_of_kind", value);
		}
	}
	
	// 5 of a kind
	private void check5OK(Dice[] local_dice) {
		
		pair_holder = new ArrayList<Integer>(); 
		
		for (Entry<Integer, Integer> row : occurance_holder.entrySet()) {
			
			if (row.getValue() == 5) {
				pair_holder.add(row.getKey() * row.getValue());
			}
			
		}
		
		// Sorting pairs 
		Collections.sort(pair_holder, Collections.reverseOrder());
		
		if (pair_holder.isEmpty() == false) {
			
			int value = pair_holder.get(0);
			
			classification_holder.put("five_of_kind", value);
		}
	}
	
	// Small straight
	private void checkSS(Dice[] local_dice) {
		
		int local_score = 0;
		
		if (occurance_holder.get(5) == 1 && occurance_holder.get(4) == 1 && occurance_holder.get(3) == 1 && occurance_holder.get(2) == 1 && occurance_holder.get(1) == 1) {
			local_score = 15;
		}
		
		if (local_score > 0) {
			classification_holder.put("small_straight", local_score);
		}
		
	}
	
	// Large straight
	private void checkLS(Dice[] local_dice) {
		
		int local_score = 0;
		
		if (occurance_holder.get(6) == 1 && occurance_holder.get(5) == 1 && occurance_holder.get(4) == 1 && occurance_holder.get(3) == 1 && occurance_holder.get(2) == 1) {
			local_score = 20;
		}
		
		if (local_score > 0) {
			classification_holder.put("large_straight", local_score);
		}
		
	}
	
	// Full straight
	private void checkFS(Dice[] local_dice) {
		
		int local_score = 0;
		
		if (occurance_holder.get(6) == 1 && occurance_holder.get(5) == 1 && occurance_holder.get(4) == 1 && occurance_holder.get(3) == 1 && occurance_holder.get(2) == 1 && occurance_holder.get(1) == 1) {
			local_score = 21;
		}
		
		if (local_score > 0) {
			classification_holder.put("full_straight", local_score);
		}
		
	}
	
	// Full house
	private void checkFH(Dice[] local_dice) {
		
		int first_3 = 0;
		int second_2 = 0;

		
		for (Entry<Integer, Integer> row : occurance_holder.entrySet()) {
			
			if (row.getValue() == 3) {
				first_3 = (row.getKey() * row.getValue());
				break;
			}
			
		}
		
		for (Entry<Integer, Integer> row : occurance_holder.entrySet()) {
			
			if (row.getValue() == 2) {
				second_2 = (row.getKey() * row.getValue());
				break;
			}
			
		}
		
		if (first_3 > 0 && second_2 > 0) {
			classification_holder.put("full_house", (first_3 + second_2));
		}
		
	}
	
	// Villa
	private void checkVilla(Dice[] local_dice) {
		
		pair_holder = new ArrayList<Integer>(); 
		
		for (Entry<Integer, Integer> row : occurance_holder.entrySet()) {
			
			if (row.getValue() == 3) {
				pair_holder.add(row.getKey() * row.getValue());
			}
			
		}
		
		// Sorting pairs 
		Collections.sort(pair_holder, Collections.reverseOrder());
		
		if (pair_holder.isEmpty() == false && pair_holder.size() >= 2) {
			
			int first_triple = pair_holder.get(0);
			int second_tripple = pair_holder.get(1);
			
			classification_holder.put("villa", (first_triple + second_tripple));
		}
		
		
	}
	
	// Tower
	private void checkTower(Dice[] local_dice) {
		
		int first_2 = 0;
		int second_4 = 0;
		
		for (Entry<Integer, Integer> row : occurance_holder.entrySet()) {
			
			if (row.getValue() == 2) {
				first_2 = (row.getKey() * row.getValue());
				break;
			}
			
		}
		
		for (Entry<Integer, Integer> row : occurance_holder.entrySet()) {
			
			if (row.getValue() == 4) {
				second_4 = (row.getKey() * row.getValue());
				break;
			}
			
		}
		
		
		if (first_2 > 0 && second_4 > 0) {
			classification_holder.put("tower", (first_2 + second_4));
		}

		
	}

	// Chance
	private void checkChance(Dice[] local_dice) {
		
		int local_score = 0;
		
		for (Dice dice : local_dice) {
			local_score += dice.getValue();
		}
		
		if (local_score > 0 ) {
			classification_holder.put("chance", local_score);
		}

	}
	
	// Maxi
	private void checkMaxi(Dice[] local_dice) {
		
		int local_score = 0;
		
		for (Entry<Integer, Integer> row : occurance_holder.entrySet()) {
			
			if (row.getValue() == 6) {
				local_score = 100;
				break;
			}
			
		}
		
		if (local_score > 0) {
			classification_holder.put("maxi_yahtzee", local_score);
		}
		
	}

	
	@Override
	public HashMap<String, Integer> returnDeltaCombinations(HashMap<String, Integer> contestant_list, HashMap<String, Integer> possible_combinations, boolean yahtzee_flag) {
		
		HashMap<String, Integer> delta_combinations = new HashMap<String, Integer>();
		
		// Iterating through both lists for matching
		for (Entry<String, Integer> combo : contestant_list.entrySet()) {
			
			String combo_name = combo.getKey();
			
			for (Entry<String, Integer> possible_combo : possible_combinations.entrySet()) {
				
				String possible_combo_name = possible_combo.getKey();
				
				// Checking if key matches (combination name)
				if (combo_name.compareTo(possible_combo_name) == 0) {
					
					// If combination is not used
					if (combo.getValue() == null) {
						delta_combinations.put(possible_combo.getKey(), possible_combo.getValue());
					} 
					// if combination is maxi_yahtzee and it is not equal to null -> change flag (will affect user applying of combo)
					// TODO: hmm
					else if (combo_name == "maxi_yahtzee" && combo.getValue() != null) {
						yahtzee_flag = true;
					}
					
				}
				
			}
		}
		
		// If no suitable combination found -> allocate those combinations that are not used and put a zero value
		if (delta_combinations.isEmpty()) {
			
			for (Entry<String, Integer> combo : contestant_list.entrySet()) {
								
				// If combination is not used
				if (combo.getValue() == null) {
					delta_combinations.put(combo.getKey(), 0);
				}
				
			}
			
		}
		
		return delta_combinations;
	}

	@Override
	public HashMap<String, Integer> summarizePoints(HashMap<String, Integer> contestant_list) {		
		// Combo names
		String[] upper_combo_names = {"one", "two", "three", "four", "five", "six"};
		String[] lower_combo_names = {"one_pair", "two_pair", "three_pair", "three_of_kind", "four_of_kind", "five_of_kind", "small_straight", "large_straight", "full_straight", "full_house", "villa", "tower", "chance", "maxi_yahtzee"};
		String yahtzee_occurrance = "yahtzee_occurrance";
		
		// Sum holders
		int sum_upper = 0;
		int sum_lower = 0;
		
		// Calculating upper bounds
		for (String upper_combo : upper_combo_names) {
			
			for (Entry<String, Integer> combo : contestant_list.entrySet()) {
				if (combo.getKey().compareTo(upper_combo) == 0) {
					
					if (combo.getValue() == null) {
						continue;
					} else {
						sum_upper += combo.getValue();
					}
					break;
				}
			}
			
		}
		
		// Calculating lower bounds
		for (String lower_combo : lower_combo_names) {
			
			for (Entry<String, Integer> combo : contestant_list.entrySet()) {
				if (combo.getKey().compareTo(lower_combo) == 0) {
					
					if (combo.getValue() == null) {
						continue;
					} else {
						sum_upper += combo.getValue();
					}
					break;
				}
			}
			
		}
		
		// Conditions for bounds
		if (sum_upper > 83) {
			contestant_list.put("upper_bonus", 100);
			contestant_list.put("upper_total", (sum_upper + 100));
		}
		else {
			contestant_list.put("upper_bonus", 0);
			contestant_list.put("upper_total", sum_upper);
		}
		
		int yahtzee_occur = contestant_list.get(yahtzee_occurrance);
		
		sum_lower += yahtzee_occur * 100;
		
		contestant_list.put("lower_bonus", (yahtzee_occur * 100));
		contestant_list.put("lower_total", sum_lower);
		
		contestant_list.put("grand_total", (contestant_list.get("upper_total") + contestant_list.get("lower_total")));
				
		return contestant_list;
	}
	
	

}
