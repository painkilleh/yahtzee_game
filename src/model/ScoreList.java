package model;

import java.util.HashMap;

public abstract class ScoreList {
	
	public abstract HashMap<String, Integer> getScoreList();
	
	protected abstract void initScorelist(HashMap<String, Integer> empty_scorelist);
}
