package model;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map.Entry;
import java.util.Observable;

import view.GameView;

public abstract class Contestant extends Observable {
	
	// Control-variables
	private ContestantDefaultVariables CDV = new ContestantDefaultVariables();
	
	// Class default variables
	protected String name = null;
	protected HashMap<String,Integer> scorelist = null;
	private int max_rerolls = CDV.getDefaultMaxRerollValue();
	private int current_reroll = CDV.getDefaultRerollValue();
	
	// Local-game variable of the class
	protected boolean yahtzee_flag = false;
	
	// User interaction
	protected GameView GV = new GameView();
	
	// State control
	protected boolean isActive = super.hasChanged();
	protected boolean inGame = false;
	
	public Contestant(String name) {
		this.name = name;
	}
	
	public Contestant() {
	}
	
	// Checking for yahtzee occurrence
	protected void checkYahtzeeFlag() {
		
		for(Entry<String, Integer> score_row : this.scorelist.entrySet()) {
			if (score_row.getKey().equals("yahtzee") && score_row.getValue() != null) {
				this.yahtzee_flag = true;
				break;
			}
		}
		
	}
	
	// For observer via parent-class
	public void setChanged() {
		super.setChanged();
	}
	
	public boolean isActive() {
		return isActive;
	}
	
	public int getCurrentReroll() {
		return current_reroll;
	}

	public void setCurrentReroll(int current_reroll) {
		this.current_reroll = current_reroll;
	}

	public int getMaxRerolls() {
		return this.max_rerolls;
	}
	
	public void setMaxRerolls(int value) {
		this.max_rerolls = value;
	}
		
	public void initializeScoreList(HashMap<String, Integer> scorelist) {
		
		if (this.scorelist == null) {
			this.scorelist = new HashMap<String, Integer>();
		}
		

		for (Entry<String, Integer> row : scorelist.entrySet()) {
			
			if (row.getValue() == null) {
				this.scorelist.put(row.getKey(), null);
			} else {
				this.scorelist.put(row.getKey(), row.getValue());
			}
			
		}
		
	}
	
	public String getName() {
		return this.name;
	}
	
	public void setName(String name) {
		this.name = name;
	}

	public HashMap<String,Integer> getScoreList() {
		return this.scorelist;
	}
	
	// Roll all dices
	public void rollDices(Dice[] dices) {
		
		for (Dice d: dices) {
			d.rollDice();
		}
		
	}
	
	// Roll selected ones
	public void rollDices(ArrayList<Integer> indexes_of_dices, Dice[] dices) {
		
		for (int i = 0; i < indexes_of_dices.size(); i++) {
			int dice_index = indexes_of_dices.get(i);
			dices[dice_index].rollDice();
		}
		
	}
	
	// Apply combination
	public void applyCombination(int combo_index, HashMap<String, Integer> delta_combo_list) {
		// Local inits
		
		int local_counter = 0;
		String combo_name = null;
		int combo_value = 0;
		
		for (Entry<String, Integer> entry : delta_combo_list.entrySet()) {
			
			if (local_counter == combo_index) {
				combo_name = entry.getKey();
				combo_value = entry.getValue();
				break;
			}
			local_counter++;
			
		}
		
		// Yahtzee multiple occurrence
		if (this.yahtzee_flag) {
									
			for (Entry<String, Integer> row : this.scorelist.entrySet()) {
				
				// Looking for specific row in scorelist
				if (row.getKey().compareTo("yahtzee_occurrance") == 0) {
					row.setValue(row.getValue() + 1);
					this.yahtzee_flag = false;
					break;
				}
				
			}
			
		}
		
		// Replacing the key+value in score list		
		this.scorelist.replace(combo_name, combo_value);
		
	}
	
}
