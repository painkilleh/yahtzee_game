package model;

import java.util.HashMap;
import java.util.Map.Entry;

public abstract class ScoreClassifier {
	
	protected HashMap<String, Integer> classification_holder = null;
	protected HashMap<Integer, Integer> occurance_holder = null;
	
	// function that returns possible combinations, based on dices' values
	protected abstract HashMap<String, Integer> showPossibleCombinations(Dice[] dices);
	
	// function that return delta combinations 
	protected abstract HashMap<String, Integer> returnDeltaCombinations(HashMap<String, Integer> contestant_list, HashMap<String, Integer> possible_combinations, boolean yahtzee_flag);
	
	// function that calculates bonus points for lower/upper bounds
	protected abstract HashMap<String, Integer> summarizePoints(HashMap<String, Integer> contestant_list);
		
	// Bubble-sorting (DESC)
	protected void sortDices(Dice[] dice) {
		
		Dice temp_dice;
		
		for(int i = 0; i < dice.length; i++) {
			for(int j = 1; j < (dice.length - i); j++) {
				if(dice[j - 1].getValue() < dice[j].getValue()) {
					temp_dice = dice[j-1];
					dice[j-1] = dice[j];
					dice[j] = temp_dice;
				}
			}
		}
	}
	
	// Initialize occurrence list - will be used for lower bound combos
	protected void initializeOccurrenceList() {
		
		for (int i = 1; i <= 6; i++) {
			occurance_holder.put(i, 0);
		}

	}
	
	// Count occurrences of dices
	protected void countOccurrences(Dice[] dices) {
		
		for (Dice dice : dices) {
			
			int dice_value = dice.getValue();
			
			for (Entry<Integer, Integer> row : occurance_holder.entrySet()) {
				
				if (dice_value == row.getKey()) {
					row.setValue(row.getValue() + 1);
					break;
				}
				
			}
			
		}
		
	}
	
}
