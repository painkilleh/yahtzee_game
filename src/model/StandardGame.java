package model;

import java.util.Iterator;
import java.util.List;
import java.util.Observable;

public class StandardGame extends Game {
	
	
	public StandardGame(List<Contestant> contestants, ScoreList SL, ScoreClassifier SC) {
		super(contestants, SL, SC);
		this.max_turns = this.GCV.getStandard_game_max_turns();
		this.amount_of_dices = this.GCV.getStandard_game_dice_count();
	}


	public StandardGame(List<Contestant> contestants, Dice[] dices, int entered_turn, ScoreList SL, ScoreClassifier SC) {
		super(contestants, dices, entered_turn, SL, SC);
		this.max_turns = this.GCV.getStandard_game_max_turns();
	}
		
	@Override
	public void update(Observable cont, Object arg) {
		
		cont = (Contestant) cont;
		((Contestant)cont).inGame = true;
		((Contestant)cont).checkYahtzeeFlag();
		
		int action_result = 0;
		
		if (cont instanceof Player) {
			action_result = ((Player) cont).provideUserInput(this.dices, ((Player) cont).yahtzee_flag, this.score_classifier);
		} else if (cont instanceof Bot) {
			action_result = ((Bot) cont).executeLogic(this.dices, ((Bot) cont).yahtzee_flag, this.score_classifier);
		}
		
		if (action_result == 1) {
			this.saveTheGameandExit();
		} else if (action_result == 2) {
			this.exitTheGame();
		}
		
	}
	
	@Override
	public void startTheGame() {
		
		if (this.isNewGame == true) {
			this.initializeDices(this.amount_of_dices);
			this.initializeScoreListsForContestants();
			this.decideOrder(this.getContestants());
		}
			
		
		// Global game control
		while (this.current_turn <= this.max_turns) {
			
			// Loop variables
			Iterator<Contestant> cont_iter = contestants.iterator();
			boolean canFollow = cont_iter.hasNext();
			
			this.GV.displayGameTurn(this.current_turn, this.max_turns);
			
			while (canFollow) {
				Contestant next_contestant = cont_iter.next();
								
				next_contestant.setChanged();
				next_contestant.notifyObservers();
				
				canFollow = cont_iter.hasNext();
				
				// Forcing re-roll
				for (Dice d: this.dices) {
					d.rollDice();
				}
				
				// Re-initializing flag 
				next_contestant.inGame = false;
				next_contestant.setCurrentReroll(GCV.getDefaultRerollValue());
				
			}
			
			this.current_turn++;
				
		}
		
		// Finished game operations
		this.status = GCV.setFinishedGameStatus();
		GV.displayEndGameMessage(status);
		
		for (Contestant cont : contestants) {
			score_classifier.summarizePoints(cont.getScoreList());
		}
		
		this.ioxml.saveTheGame(this);
		this.MC.execute();
		
	}

}
