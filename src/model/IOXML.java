package model;

import java.io.File;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;
import javax.xml.transform.OutputKeys;
import javax.xml.transform.Transformer;
import javax.xml.transform.TransformerException;
import javax.xml.transform.TransformerFactory;
import javax.xml.transform.dom.DOMSource;
import javax.xml.transform.stream.StreamResult;

import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;

import view.MainView;

public class IOXML {
		
	private String path = "save";
	
	protected MainView MV = new MainView();
    private GameFactory GF = new GameFactory();
	
	public IOXML() {
		this.initDir();
	};
	
	// Class for storing 
	public class simpleData {
		
		public String date;
		public String status;
		public String name;
		public String grand_total;

		public simpleData(String date, String status, String name, String grand_total) {
			this.name = name;
			this.grand_total = grand_total;
			this.date = date;
			this.status = status;
		}
		
	}
	
	public class extendedData {
		
		public String name;
		public String status;
		public HashMap<String, Integer> scorelist;
		
		public extendedData(String name, String status, HashMap<String, Integer> scorelist) {
			this.name = name;
			this.scorelist = scorelist;
			this.status = status;
		}
		
	}
	
	public Map<String, simpleData[]> verboseSimple() {

		File folder = new File(path);
		File[] listFiles = folder.listFiles();

		Map<String, simpleData[]> listContents = new LinkedHashMap<String, simpleData[]>();

		for (File file : listFiles) {
			
			simpleData[] temp_storage = null;

			try {
				DocumentBuilderFactory dbFactory = DocumentBuilderFactory.newInstance();
				DocumentBuilder dBuilder = dbFactory.newDocumentBuilder();
				Document doc = dBuilder.parse(file);

				String date = doc.getElementsByTagName("date").item(0).getTextContent();
				
				String status = doc.getElementsByTagName("status").item(0).getTextContent();

				NodeList contestants_block = doc.getElementsByTagName("contestants").item(0).getChildNodes();
				
			    Element contestants_elements = (Element) contestants_block;
			    NodeList c_list = contestants_elements.getElementsByTagName("contestant");
			    
			    temp_storage = new simpleData[c_list.getLength()];
			    
			    for (int i = 0 ; i < c_list.getLength(); i++) {
			    	
			    	Element selected_contestant = (Element) c_list.item(i);
			    	
			    	String name = selected_contestant.getElementsByTagName("name").item(0).getTextContent();
			    	
			    	NodeList score_list = selected_contestant.getElementsByTagName("score_list").item(0).getChildNodes();
			    	
			    	Element temp = (Element) score_list;
			    	
			    	String grand_total = temp.getElementsByTagName("grand_total").item(0).getTextContent();
			    	
					simpleData sData = new simpleData(date, status, name, grand_total);
			    	
			    	temp_storage[i] = sData;
			    	
			    }
			    
			    listContents.put(file.getName(), temp_storage);
			
			} catch (Exception e) {
				e.printStackTrace();
			}
		}
		
		return listContents;
	}
			
	public Map<String, extendedData[]> verboseExtended() {
		
		File folder = new File(path);
		File[] listFiles = folder.listFiles();
		
		Map<String, extendedData[]> listContents = new LinkedHashMap<String, extendedData[]>();
		
		for (File file : listFiles) {
			
			extendedData[] temp_storage = null;
			
			try {
			    DocumentBuilderFactory dbFactory = DocumentBuilderFactory.newInstance();
			    DocumentBuilder dBuilder = dbFactory.newDocumentBuilder();
			    Document doc = dBuilder.parse(file);
			    
			    NodeList contestants_block = doc.getElementsByTagName("contestants").item(0).getChildNodes();
			    
				String status = doc.getElementsByTagName("status").item(0).getTextContent();
				
			    Element contestants_elements = (Element) contestants_block;			    
			    NodeList c_list = contestants_elements.getElementsByTagName("contestant");
			    
			    temp_storage = new extendedData[c_list.getLength()];
			    
			    for (int i = 0; i < c_list.getLength(); i++) {
			    	
			    	Element selected_contestant = (Element) c_list.item(i);
			    	
			    	String name = selected_contestant.getElementsByTagName("name").item(0).getTextContent();
			    	
			    	NodeList score_list = selected_contestant.getElementsByTagName("score_list").item(0).getChildNodes();
			    	
			    	HashMap<String, Integer> temp_sl = new HashMap<String, Integer>();
			    	
			    	for (int j = 0; j < score_list.getLength(); j++) {
			    		
			    		Node node = score_list.item(j);
			    		
			    		if (node.getNodeType() == Node.ELEMENT_NODE) {
			    			
			    			if (node.getTextContent().compareTo("x") == 0) {
			    				temp_sl.put(node.getNodeName(), null);
			    			} else {
			    				temp_sl.put(node.getNodeName(), Integer.parseInt(node.getTextContent()));
			    			}
			    			
			    		}
			    		
			    	}
			    	
			    	extendedData eData = new extendedData(name, status, temp_sl);
			    	temp_storage[i] = eData;
			    	
			    }

			    listContents.put(file.getName(), temp_storage);
			    
			   }
			catch (Exception e) {
				e.printStackTrace();
			}
			    
			}
			
			return listContents;
	}
	
	private String provideExtension(String path) {
		
		String extension = "";

		int i = path.lastIndexOf('.');
		if (i > 0) {
		    extension = path.substring(i+1);
		}
		
		return extension;
	}

	// Creates and checks a directory if it can be created -> creates one
	private void initDir() {
		File directory = null;
		
		try {
			
			directory = new File(path);
			directory.mkdir();

		}
		
		catch (Exception e) {
			e.printStackTrace();
		}

	}
	
	// Save the current game
	public void saveTheGame(Game game) {
		
		boolean unfinished = false;
		
		try {
			// Inits
	        DocumentBuilderFactory docFactory = DocumentBuilderFactory.newInstance();
	        DocumentBuilder docBuilder = docFactory.newDocumentBuilder();
	        
	        // Creating root
	        Document doc = docBuilder.newDocument();
	        Element rootElement = doc.createElement("game_file");
	        doc.appendChild(rootElement); 
	        
	        Element general_block = doc.createElement("general");
	        rootElement.appendChild(general_block);
	             
	        // Creating date
	        SimpleDateFormat sdf = new SimpleDateFormat("dd-MM-yyyy HH:mm:ss");
	        String date = sdf.format(new Date());
	        Element date_block = doc.createElement("date");
	        date_block.appendChild(doc.createTextNode(date));
	        
	        // Creating game ID
	        int game_id = Math.abs(date.hashCode());
	        Element game_id_block = doc.createElement("game_id");
	        game_id_block.appendChild(doc.createTextNode(Integer.toString(game_id)));
	        
	        // Extracting status
	        String status = game.getStatus();
	        Element status_block = doc.createElement("status");
	        status_block.appendChild(doc.createTextNode(status));
	        
	        // Setting the type of game 
	        String type_of_game = null;
	        
	        if (game instanceof StandardGame) {
	        	type_of_game = "standard";
	        } else if (game instanceof MaxiGame) {
	        	type_of_game = "maxi";
	        }
	        
	        Element tog_block = doc.createElement("type");
	        tog_block.appendChild(doc.createTextNode(type_of_game));
	        
	        // Appending general block to root 
	        general_block.appendChild(game_id_block);
	        general_block.appendChild(date_block);
	        general_block.appendChild(status_block);
	        general_block.appendChild(tog_block);
	        
	        
	        // Adding section if game is unfinished
	        if (status.compareTo("unfinished") == 0) {	
	        	
	        	unfinished = true;
	        	
		        // Game flow block
		        Element game_flow_block = doc.createElement("game_flow");
		        
		        // New block of info -> for game control flag
		        int current_turn = game.getCurrentTurn();
		        Element ct_block = doc.createElement("current_turn");
		        ct_block.appendChild(doc.createTextNode(Integer.toString(current_turn)));
		        
		        // Extracting dice value
		        Dice[] saved_dices = game.getDices();
		        Element dices_block = doc.createElement("dices");
		        
		        for (Dice dice : saved_dices) {
		        	Element dice_block = doc.createElement("dice");
		        	int dice_value = dice.getValue();
		        	dice_block.appendChild(doc.createTextNode(Integer.toString(dice_value)));
		        	dices_block.appendChild(dice_block);
		        }
		        
		        
		        // Appending game flow variables to game flame block
		        game_flow_block.appendChild(ct_block);
		        game_flow_block.appendChild(dices_block);
		        rootElement.appendChild(game_flow_block); 
	        }
	        

	        // Contestants information
	        Element contestants_block = doc.createElement("contestants");
	        
	        List<Contestant> list_contestants = game.getContestants();
	        
	        for (Contestant contestant : list_contestants) {
	        	
	        	Element cont_block = doc.createElement("contestant");
	        	Element name_block = doc.createElement("name");
	        	Element type_block = doc.createElement("type");
	        	
	        	// Recoving data about last man turn in game
	        	
	        	if (contestant.inGame && unfinished) {
	        		Element active_block = doc.createElement("last");
	        		active_block.appendChild(doc.createTextNode(Boolean.toString(contestant.inGame)));
	        		cont_block.appendChild(active_block);
	        	}
	        	
	        	// if the game is unfinished
	        	if (unfinished) {
	        		
	        		Element current_reroll_block = doc.createElement("current_reroll");
	        		current_reroll_block.appendChild(doc.createTextNode(Integer.toString(contestant.getCurrentReroll())));
	        		
	        		Element max_reroll_block = doc.createElement("max_rerolls");	
		        	max_reroll_block.appendChild(doc.createTextNode(Integer.toString(contestant.getMaxRerolls())));
		        	
		        	cont_block.appendChild(current_reroll_block);
		        	cont_block.appendChild(max_reroll_block);
		        	
	        	}
	        	
	        	String name = contestant.getName();
	        	name_block.appendChild(doc.createTextNode(name));
	        	
	        	String type = null;
	        	if (contestant instanceof Player) {
	        		type = "player";
	        	} else if (contestant instanceof Bot) {
	        		type = "bot"; 
	        	}
	        	
	        	type_block.appendChild(doc.createTextNode(type));
	        	
	        	cont_block.appendChild(name_block);
	        	cont_block.appendChild(type_block);
	        	
	        	HashMap<String, Integer> temp_SL = contestant.getScoreList();
	        	
	        	Element score_block = doc.createElement("score_list");
	        	
	        	for (Entry<String, Integer> row : temp_SL.entrySet()) {
	        		
	        		Element row_block = doc.createElement(row.getKey());
	        		
	        		if (row.getValue() == null) {
	        			row_block.appendChild(doc.createTextNode("x"));
	        		}
	        		else {
	        			row_block.appendChild(doc.createTextNode(Integer.toString(row.getValue())));
	        		}
	        		
	        		score_block.appendChild(row_block);
	    
	        	}
	        	
	        	cont_block.appendChild(score_block);
	        	contestants_block.appendChild(cont_block);
	        	
	        }
	        
	        rootElement.appendChild(contestants_block);
	        

	        
	         // write the content into xml file
	         TransformerFactory transformerFactory = TransformerFactory.newInstance();
	         Transformer transformer = transformerFactory.newTransformer();
	         transformer.setOutputProperty(OutputKeys.INDENT, "yes");
	         DOMSource source = new DOMSource(doc);
	         
	         String final_product = path + "\\" + "game_" + Integer.toString(game_id) + ".xml";
	         
	         StreamResult result = new StreamResult(new File(final_product));
	         transformer.transform(source, result);
	        
			
		} catch (ParserConfigurationException pce) {
			pce.printStackTrace();
		} catch (TransformerException tfe) {
			tfe.printStackTrace();
		}
		
	}
	
	
	// Load the game
	public void loadTheGame(String game_file_path) {
		
		File game_file = null;
		String extension = this.provideExtension(game_file_path);
		
		// Holders for game inits
		String game_type = null;
		String current_turn = null;
		int indexofLast = 0;
		Dice[] dices = null;
		List<Contestant> listContestant = new ArrayList<Contestant>();
		HashMap<String, Integer> temp_scorelist = null;
		
		try {

			game_file = new File(game_file_path);
			
			if (extension.compareTo("xml") == 0) {
				
			    DocumentBuilderFactory dbFactory = DocumentBuilderFactory.newInstance();
			    DocumentBuilder dBuilder = dbFactory.newDocumentBuilder();
			    
			    String correct_path = path + "\\" + game_file.getName();
			    
			    Document doc = dBuilder.parse(correct_path);
			    
			    NodeList general_list = doc.getElementsByTagName("general").item(0).getChildNodes();
			    
		    	// Retrieving type of the game -> for controller
			    for (int i = 0; i < general_list.getLength(); i++) {
			    	Node node = general_list.item(i);
			    	
			    	if (node.getNodeType() == Node.ELEMENT_NODE) {
			    		
			    		Element el = (Element) node;
			    		
			    		if (el.getNodeName() == "type") {
			    			game_type = el.getTextContent();
			    			break;
			    		}
			    	}
			    }
			    
			    // Retrieving the global turn count
			    NodeList game_flow = doc.getElementsByTagName("game_flow").item(0).getChildNodes();
			    
			    for (int j = 0; j < game_flow.getLength(); j++) {
			    	Node node = game_flow.item(j);
			    	
			    	if (node.getNodeType() == Node.ELEMENT_NODE) {
			    		
			    		Element el = (Element) node;
			    		
			    		if (el.getNodeName() == "current_turn") {
			    			
			    			current_turn = el.getTextContent();
			    			break;
			    		}
			    	}
			    	
			    }
			    
			    // Retrieving the dices
			    NodeList dices_block = doc.getElementsByTagName("dices").item(0).getChildNodes();
			    
			    // Reading for first time to acquire length for dices
			    int actual_size = 0;
			    
			    for (int q = 0; q < dices_block.getLength(); q++) {
			    	
			    	Node node = dices_block.item(q);
			    	
			    	if (node.getNodeType() == Node.ELEMENT_NODE) {			    			    		
			    		actual_size++;
			    	}
			 
			    }
			    
			    // Creating fixed size array
			    dices = new Dice[actual_size];
			    int local_dice_index = 0;
			    
			    // TODO: here
			    for (int j = 0; j < dices_block.getLength(); j++) {
			    	
			    	Node node = dices_block.item(j);
			    				    		
			    	if (node.getNodeType() == Node.ELEMENT_NODE) {		
			    			// Integer.parseInt(el.getTextContent())
			    			Dice loaded_dice = new Dice(Integer.parseInt(node.getTextContent()));
			    			// loaded_dice.setDiceValue(Integer.parseInt(node.getTextContent()));
			    			//dices[local_dice_index] = new Dice(Integer.parseInt(node.getTextContent()));
			    			dices[local_dice_index] = loaded_dice;
							local_dice_index++;
			    	}
			    	
			    }
			    			    			    
			    NodeList contestants_block = doc.getElementsByTagName("contestants").item(0).getChildNodes();	
			    Element contestants_elements = (Element) contestants_block;
			    NodeList c_list = contestants_elements.getElementsByTagName("contestant");
			    
			    int counter = 0;
			    
			    // First iteration to find last person that saved the game -> he will become a fresh start for the loaded game
			    for (int k = 0; k < c_list.getLength(); k++) {
			    	
			    	Node node = c_list.item(k);
			    	
			    	if (node.getNodeType() == Node.ELEMENT_NODE) {
			    		
			    		Element selected_element = (Element) node;
			    		
			    		if (selected_element.getElementsByTagName("last").item(0) != null && selected_element.getElementsByTagName("last").item(0).getTextContent().compareTo("true") == 0) {
		    				
			    			indexofLast = counter;
			    			
			    			String type = selected_element.getElementsByTagName("type").item(0).getTextContent();
		    				
		    				
		    				if (type.compareTo("player") == 0) {
		    					
				    			String name = selected_element.getElementsByTagName("name").item(0).getTextContent();
			    				int current_reroll = Integer.parseInt(selected_element.getElementsByTagName("current_reroll").item(0).getTextContent());
			    				int max_reroll = Integer.parseInt(selected_element.getElementsByTagName("max_rerolls").item(0).getTextContent());
			    				
			    				Player player = new Player(name);
			    				
			    				player.setCurrentReroll(current_reroll);
			    				player.setMaxRerolls(max_reroll);
			    				
			    				temp_scorelist = new HashMap<String, Integer>();
			    				
			    				NodeList score_list_nodes = selected_element.getElementsByTagName("score_list").item(0).getChildNodes();
			    				
			    				for (int q = 0; q < score_list_nodes.getLength(); q++) {
			    					
			    					if (score_list_nodes.item(q).getNodeType() == Node.ELEMENT_NODE) {
			    						
			    						String combo_name = score_list_nodes.item(q).getNodeName();
			    						
			    						String temp_value = score_list_nodes.item(q).getTextContent();
			    						
			    						
			    						if (temp_value.compareTo("x") == 0) {
			    							temp_scorelist.put(combo_name, null);
			    						} else {
			    							temp_scorelist.put(combo_name, Integer.parseInt(temp_value));
			    						}
			    						
			    					}
			    					
			    				}
			    				
			    				player.initializeScoreList(temp_scorelist);
			    				listContestant.add(player);
		    					
		    				} else if (type.compareTo("bot") == 0) {
		    							
			    				Bot bot = new Bot();
			    				
			    				String name = selected_element.getElementsByTagName("name").item(0).getTextContent();
			    				int current_reroll = Integer.parseInt(selected_element.getElementsByTagName("current_reroll").item(0).getTextContent());
			    				int max_reroll = Integer.parseInt(selected_element.getElementsByTagName("max_rerolls").item(0).getTextContent());
			    				
			    				bot.setName(name);
			    				bot.setCurrentReroll(current_reroll);
			    				bot.setMaxRerolls(max_reroll);
			    				
			    				temp_scorelist = new HashMap<String, Integer>();
			    				
			    				NodeList score_list_block = selected_element.getElementsByTagName("score_list").item(0).getChildNodes();
			    				
								for (int q = 0; q < score_list_block.getLength(); q++) {

									if (score_list_block.item(q).getNodeType() == Node.ELEMENT_NODE) {

										String combo_name = score_list_block.item(q).getNodeName();

										String temp_value = score_list_block.item(q).getTextContent();

										if (temp_value.compareTo("x") == 0) {
											temp_scorelist.put(combo_name, null);
										} else {
											temp_scorelist.put(combo_name, Integer.parseInt(temp_value));
										}

									}

								}
			    				
			    				bot.initializeScoreList(temp_scorelist);
			    				listContestant.add(bot);
		    					
		    				}
			    			
			    			break;
			    		}
			    		else {
			    			counter++;
			    		}
			    		
			    	}
			    	
			    }
			    				    	
			   
			    // Appending the following contestants after the last person
			    // Last saved man is not last in the queue
			    if (indexofLast < c_list.getLength()) {
			    	
				    for (int a = indexofLast + 1; a < c_list.getLength(); a++) {
				    	
				    	Node selected_node = c_list.item(a);
				    	
				    	if (selected_node.getNodeType() == Node.ELEMENT_NODE) {
				    		
		    				Element selected_element = (Element) selected_node;
		    				
		    				String type = selected_element.getElementsByTagName("type").item(0).getTextContent();
		    						    				
		    				if (type.compareTo("player") == 0) {
		    					
				    			String name = selected_element.getElementsByTagName("name").item(0).getTextContent();
			    				int current_reroll = Integer.parseInt(selected_element.getElementsByTagName("current_reroll").item(0).getTextContent());
			    				int max_reroll = Integer.parseInt(selected_element.getElementsByTagName("max_rerolls").item(0).getTextContent());
			    				
			    				Player player = new Player(name);
			    				
			    				player.setCurrentReroll(current_reroll);
			    				player.setMaxRerolls(max_reroll);
			    				
			    				temp_scorelist = new HashMap<String, Integer>();
			    				
			    				NodeList score_list_nodes = selected_element.getElementsByTagName("score_list").item(0).getChildNodes();
			    				
			    				for (int q = 0; q < score_list_nodes.getLength(); q++) {
			    					
			    					if (score_list_nodes.item(q).getNodeType() == Node.ELEMENT_NODE) {
			    						
			    						String combo_name = score_list_nodes.item(q).getNodeName();
			    						
			    						String temp_value = score_list_nodes.item(q).getTextContent();
			    						
			    						
			    						if (temp_value.compareTo("x") == 0) {
			    							temp_scorelist.put(combo_name, null);
			    						} else {
			    							temp_scorelist.put(combo_name, Integer.parseInt(temp_value));
			    						}
			    						
			    					}
			    					
			    				}
			    				
			    				player.initializeScoreList(temp_scorelist);
			    				listContestant.add(player);
		    					
		    				} else if (type.compareTo("bot") == 0) {
		    							
			    				Bot bot = new Bot();
			    				
			    				String name = selected_element.getElementsByTagName("name").item(0).getTextContent();
			    				int current_reroll = Integer.parseInt(selected_element.getElementsByTagName("current_reroll").item(0).getTextContent());
			    				int max_reroll = Integer.parseInt(selected_element.getElementsByTagName("max_rerolls").item(0).getTextContent());
			    				
			    				bot.setName(name);
			    				bot.setCurrentReroll(current_reroll);
			    				bot.setMaxRerolls(max_reroll);
			    				
			    				temp_scorelist = new HashMap<String, Integer>();
			    				
			    				NodeList score_list_nodes = selected_element.getElementsByTagName("score_list").item(0).getChildNodes();
			    				
								for (int q = 0; q < score_list_nodes.getLength(); q++) {

									if (score_list_nodes.item(q).getNodeType() == Node.ELEMENT_NODE) {

										String combo_name = score_list_nodes.item(q).getNodeName();

										String temp_value = score_list_nodes.item(q).getTextContent();

										if (temp_value.compareTo("x") == 0) {
											temp_scorelist.put(combo_name, null);
										} else {
											temp_scorelist.put(combo_name, Integer.parseInt(temp_value));
										}

									}

								}
			    				
			    				bot.initializeScoreList(temp_scorelist);
			    				listContestant.add(bot);
		    					
		    				}
				    	}
				   	
				    }
				    
			    }
			    
			    
			    if (indexofLast != 0) {
			    	
				    for (int a = 0; a < indexofLast; a++) {
				    	
				    	Node selected_node = c_list.item(a);
				    	
				    	if (selected_node.getNodeType() == Node.ELEMENT_NODE) {
				    		
		    				Element selected_element = (Element) selected_node;
		    				
		    				String type = selected_element.getElementsByTagName("type").item(0).getTextContent();
		    				
		    				if (type.compareTo("player") == 0) {
		    					
				    			String name = selected_element.getElementsByTagName("name").item(0).getTextContent();
			    				int current_reroll = Integer.parseInt(selected_element.getElementsByTagName("current_reroll").item(0).getTextContent());
			    				int max_reroll = Integer.parseInt(selected_element.getElementsByTagName("max_rerolls").item(0).getTextContent());
			    				
			    				Player player = new Player(name);
			    				
			    				player.setCurrentReroll(current_reroll);
			    				player.setMaxRerolls(max_reroll);
			    				
			    				temp_scorelist = new HashMap<String, Integer>();
			    				
			    				NodeList score_list_nodes = selected_element.getElementsByTagName("score_list").item(0).getChildNodes();
			    				
			    				for (int q = 0; q < score_list_nodes.getLength(); q++) {
			    					
			    					if (score_list_nodes.item(q).getNodeType() == Node.ELEMENT_NODE) {
			    						
			    						String combo_name = score_list_nodes.item(q).getNodeName();
			    						
			    						String temp_value = score_list_nodes.item(q).getTextContent();
			    						
			    						
			    						if (temp_value.compareTo("x") == 0) {
			    							temp_scorelist.put(combo_name, null);
			    						} else {
			    							temp_scorelist.put(combo_name, Integer.parseInt(temp_value));
			    						}
			    						
			    					}
			    					
			    				}
			    				
			    				player.initializeScoreList(temp_scorelist);
			    				listContestant.add(player);
		    					
		    				} else if (type.compareTo("bot") == 0) {
		    							
			    				Bot bot = new Bot();
			    				
			    				String name = selected_element.getElementsByTagName("name").item(0).getTextContent();
			    				int current_reroll = Integer.parseInt(selected_element.getElementsByTagName("current_reroll").item(0).getTextContent());
			    				int max_reroll = Integer.parseInt(selected_element.getElementsByTagName("max_rerolls").item(0).getTextContent());
			    				
			    				bot.setName(name);
			    				bot.setCurrentReroll(current_reroll);
			    				bot.setMaxRerolls(max_reroll);
			    				
			    				temp_scorelist = new HashMap<String, Integer>();
			    				
			    				NodeList score_list_block = selected_element.getElementsByTagName("score_list").item(0).getChildNodes();
			    				
								for (int q = 0; q < score_list_block.getLength(); q++) {

									if (score_list_block.item(q).getNodeType() == Node.ELEMENT_NODE) {

										String combo_name = score_list_block.item(q).getNodeName();

										String temp_value = score_list_block.item(q).getTextContent();

										if (temp_value.compareTo("x") == 0) {
											temp_scorelist.put(combo_name, null);
										} else {
											temp_scorelist.put(combo_name, Integer.parseInt(temp_value));
										}

									}

								}
			    				
			    				bot.initializeScoreList(temp_scorelist);
			    				listContestant.add(bot);
		    					
		    				}
				    	}
				   	
				    }
			    	
			    }
    
			    // Finally creating the controller with all required data
			    if (game_type.compareTo("standard") == 0) {
			    	
			    	Game standard_game = GF.getGame(GameType.type.standard, listContestant, dices, Integer.parseInt(current_turn));
			    	
			    	standard_game.startTheGame();
			    	
			    } else if (game_type.compareTo("maxi") == 0) {
			    	
			    	Game maxi_game = GF.getGame(GameType.type.maxi, listContestant, dices, Integer.parseInt(current_turn));
			    	
			    	maxi_game.startTheGame();
			    	
			    }
			    
				
			} else {
				MV.displayError("Could not find the file! Exiting...");
			}

		} catch (Exception e) {
			e.printStackTrace();
		}
	}
}
