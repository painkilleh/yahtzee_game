package model;

import java.util.HashMap;

public class StandardScoreList extends ScoreList {
		
	// based on https://www.wikihow.com/Play-Yahtzee
	public StandardScoreList() {
	}
		
	public HashMap<String, Integer> getScoreList() {
		HashMap<String, Integer> generated_scorelist = new HashMap<String, Integer>();
		this.initScorelist(generated_scorelist);
		return generated_scorelist;
	}
	
	protected void initScorelist(HashMap<String, Integer> empty_scorelist) {
		// Single/upper combinations
		empty_scorelist.put("one", null);
		empty_scorelist.put("two", null);
		empty_scorelist.put("three", null);
		empty_scorelist.put("four", null);
		empty_scorelist.put("five", null);
		empty_scorelist.put("six", null);
		
		// Special/lower combinations
		empty_scorelist.put("three_of_kind", null);
		empty_scorelist.put("four_of_kind", null);
		empty_scorelist.put("full_house", null);
		empty_scorelist.put("small_straight", null);
		empty_scorelist.put("large_straight", null);
		empty_scorelist.put("chance", null);
		empty_scorelist.put("yahtzee", null);
		empty_scorelist.put("yahtzee_occurrance", 0);
	}
	
}
