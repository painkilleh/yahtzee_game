package model;

public class GameControlVariables {
			
	// Variables list
	
	// Standard game
	private final static int standard_game_dice_count = 5;
	private final static int standard_game_max_turns = 13;
	
	// Maxi game
	private final static int maxi_game_max_turns = 20;
	private final static int maxi_game_dice_count = 6;
	
	
	// Default turns for the game
	private final static int start_turn_value = 1;
	
	// Game statuses
	private final static String unfinished_status = "unfinished";
	private final static String finished_status = "finished";
	
	// Default re-roll value
	private final static int default_reroll_value = 1;
	private final static int default_max_reroll_value = 3;
	
	public GameControlVariables() {
	}
	
	// Getter for control variables
	public int getStandard_game_dice_count() {
		return standard_game_dice_count;
	}

	public int getStandard_game_max_turns() {
		return standard_game_max_turns;
	}

	public int getMaxi_game_max_turns() {
		return maxi_game_max_turns;
	}

	public int getMaxi_game_dice_count() {
		return maxi_game_dice_count;
	}
	
	public int getStartTurnValue() {
		return start_turn_value;
	}
	
	public String setUnfinishedGameStatus() {
		return unfinished_status;
	}
	
	public String setFinishedGameStatus() {
		return finished_status;
	}
	
	public int getDefaultRerollValue() {
		return default_reroll_value;
	}
	
	public int getDefaultMaxRerollValue() {
		return default_max_reroll_value;
	}
		
}
 