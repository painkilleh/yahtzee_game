package model;

import java.util.Random;

public class Dice {
	
	private int value = 0;
	protected Random random = new Random();
	
	public Dice () {
		this.value = random.nextInt(6) + 1;
	}
	
	public Dice (int value) {
		this.value = value;
	}
		
	public int getValue() {
		return this.value;
	}
	
	public void rollDice() {
		this.value = random.nextInt(6) + 1;
	}
	
}
