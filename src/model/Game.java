package model;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Observer;

import controller.MainController;
import view.GameView;
import view.MainView;

public abstract class Game implements Observer {
	
	// GameControlVariable
	protected GameControlVariables GCV = new GameControlVariables();
	
	// Initials for the game
	protected List<Contestant> contestants = null;
	protected Dice[] dices = null;
	protected int current_turn = GCV.getStartTurnValue();
	protected boolean isNewGame = true;
	
	// Can be added through second constructor (loading the game)
	protected int max_turns;
	protected int amount_of_dices;
	
	// Rules
	protected ScoreList score_list = null;
	protected ScoreClassifier score_classifier = null;

	// Game status
	protected String status = GCV.setUnfinishedGameStatus();
	protected GameView GV = new GameView();
	protected MainView MV = new MainView();
	
	// Access to main menu
	protected MainController MC = new MainController();
	
	// Class for save/load functionality
	protected IOXML ioxml = new IOXML();
	
	// Constructor for new game
	Game(List<Contestant> contestants, ScoreList SL, ScoreClassifier SC) {
		this.contestants = contestants;
		this.score_list = SL;
		this.score_classifier = SC;
		this.addObservers();
	}
	
	// Constructor for loaded game
	Game(List<Contestant> contestants, Dice[] loaded_dices, int entered_turn, ScoreList SL, ScoreClassifier SC) {
		this.contestants = contestants;
		this.current_turn = entered_turn;
		this.loadDices(loaded_dices);
		this.score_list = SL;
		this.score_classifier = SC;
		this.addObservers();
		this.notNewGame();
	}
		
	private void addObservers() {
		
		for(Contestant cont : this.contestants) {
			cont.addObserver(this);
		}
		
	}
	
	// Disabling init-functions for the new game by flag
	private void notNewGame() {
		this.isNewGame = false;
	}
	
	private void loadDices(Dice[] l_dices) {
				
		this.dices = new Dice[l_dices.length];
				
		for (int i = 0; i < this.dices.length; i++) {
			this.dices[i] = new Dice(l_dices[i].getValue());
		}
			
	}
		
	protected void saveTheGameandExit() {
		
		for (Contestant cont : contestants) {
			score_classifier.summarizePoints(cont.getScoreList());
		}
		
		this.ioxml.saveTheGame(this);
		this.MC.execute();
	}
	
	protected void exitTheGame() {
		this.MC.execute();
	}
		
	// Getters/setters to acquire general game information 
	public String getStatus() {
		return this.status;
	}
	
	public Dice[] getDices() {
		return this.dices;
	}
	
	public int getCurrentTurn() {
		return this.current_turn;
	}
	
	public List<Contestant> getContestants() {
		return this.contestants;
	}
		
	public int getMaxTurns() {
		return this.max_turns;
	}
	
	protected void initializeDices(int amount) {
		
		this.dices = new Dice[amount];
		
		for (int i = 0; i < amount; i++) {
			this.dices[i] = new Dice();
		}
				
	}

	// Sort each contestant by the sum of dices in descending order
	protected void sortContestantsByValue(LinkedHashMap<Contestant, Integer> storage) {
		
		List<Map.Entry<Contestant, Integer>> entries = new ArrayList<Map.Entry<Contestant, Integer>>(storage.entrySet());
		
		Collections.sort(entries, new Comparator<Map.Entry<Contestant, Integer>>(){
			 
			@Override
			public int compare(Entry<Contestant, Integer> o1, Entry<Contestant, Integer> o2) {
				return o2.getValue() - o1.getValue();
			}
		    
		});
		
		storage.clear();
		
		for (Entry<Contestant, Integer> entry : entries) {
			storage.put(entry.getKey(), entry.getValue());
		}
		
	}
	
	// Function that decides the order of contestants
	protected void decideOrder(List<Contestant> contestants) {
		
		LinkedHashMap<Contestant, Integer> local_map = new LinkedHashMap<Contestant, Integer>();
		
		// Roll the dices and fill the local map
		for (Contestant contestant : contestants) {
			
			contestant.rollDices(this.dices);
			
			int sum = 0;
			
			for (Dice dice : this.dices) {
				sum += dice.getValue();
			}
			
			local_map.put(contestant, sum);
		}
		
		this.sortContestantsByValue(local_map);
		
		contestants.clear();
		
		for(Entry<Contestant, Integer> entry : local_map.entrySet()) {
			contestants.add(entry.getKey());
		}
		
	}
	
	// Initialise scorelists for each contestant
	protected void initializeScoreListsForContestants() {
		
		for (Contestant cont : contestants) {
			cont.initializeScoreList(this.score_list.getScoreList());
		}
		
	}
	
	public abstract void startTheGame();
}
