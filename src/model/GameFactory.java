package model;

import java.util.List;

public class GameFactory {
	
	// Local init 
	private Game game = null;
	
	// Returns appropriate game type
	public Game getGame(GameType.type type, List<Contestant> contestants) {

		if (type == GameType.type.standard) {
			game = new StandardGame(contestants, new StandardScoreList(), new StandardListClassifier());
			return game;
		}
		
		else if (type == GameType.type.maxi) {
			game = new MaxiGame(contestants, new MaxiScoreList(), new MaxiListClassifier());
			return game;
		}
		else {
			return null;
		}
	}
	
	public Game getGame(GameType.type type, List<Contestant> contestants, Dice[] dices, int selected_turn) {
		
		if (type == GameType.type.standard) {
			game = new StandardGame(contestants, dices, selected_turn, new StandardScoreList(), new StandardListClassifier());
			return game;
		}
		else if (type == GameType.type.maxi) {
			game = new MaxiGame(contestants, dices, selected_turn, new MaxiScoreList(), new MaxiListClassifier());
			return game;
		}
		else {
			return null;
		}
		
	}

}
