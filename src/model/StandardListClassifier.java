package model;

import java.util.HashMap;
import java.util.Map.Entry;

public class StandardListClassifier extends ScoreClassifier {
	
	public StandardListClassifier() {};

	public HashMap<String, Integer> showPossibleCombinations(Dice[] dices) {
		
		this.classification_holder = new HashMap<String, Integer>();
		this.occurance_holder = new HashMap<Integer, Integer>();
		
		Dice[] local_dice = dices;
		this.sortDices(local_dice);
		this.initializeOccurrenceList();
		this.countOccurrences(local_dice);
		
		// Checking for combinations 
		checkOnes(local_dice);
		checkTwos(local_dice);
		checkThrees(local_dice);
		checkFours(local_dice);
		checkFives(local_dice);
		checkSixes(local_dice);
		
		checkTOK(local_dice);
		checkFOK(local_dice);
		checkFH(local_dice);
		checkSS(local_dice);
		checkLS(local_dice);
		checkYahtzee(local_dice);
		checkChance(local_dice);
				
		return this.classification_holder;
	}
	
	// Function that calculates bonus scores and totals
	@Override
	public HashMap<String, Integer> summarizePoints(HashMap<String, Integer> contestant_list) {
		// Combo names
		String[] upper_combo_names = {"one", "two", "three", "four", "five", "six"};
		String[] lower_combo_names = {"three_of_kind", "four_of_kind", "full_house", "small_straight", "large_straight", "chance", "yahtzee"};
		String yahtzee_occurrance = "yahtzee_occurrance";
		
		// Sum holders
		int sum_upper = 0;
		int sum_lower = 0;
		
		// Calculating upper bounds
		for (String upper_combo : upper_combo_names) {
			
			for (Entry<String, Integer> combo : contestant_list.entrySet()) {
				if (combo.getKey().compareTo(upper_combo) == 0) {
					
					if (combo.getValue() == null) {
						continue;
					} else {
						sum_upper += combo.getValue();
					}
					break;
				}
			}
			
		}
		
		// Calculating lower bounds
		for (String lower_combo : lower_combo_names) {
			
			for (Entry<String, Integer> combo : contestant_list.entrySet()) {
				if (combo.getKey().compareTo(lower_combo) == 0) {
					
					if (combo.getValue() == null) {
						continue;
					} else {
						sum_upper += combo.getValue();
					}
					break;
				}
			}
			
		}
		
		// Conditions for bounds
		if (sum_upper >= 63) {
			contestant_list.put("upper_bonus", 35);
			contestant_list.put("upper_total", (sum_upper + 35));
		}
		else {
			contestant_list.put("upper_bonus", 0);
			contestant_list.put("upper_total", sum_upper);
		}
		
		int yahtzee_occur = contestant_list.get(yahtzee_occurrance);
		
		sum_lower += yahtzee_occur * 100;
		
		contestant_list.put("lower_bonus", (yahtzee_occur * 100));
		contestant_list.put("lower_total", sum_lower);
		
		contestant_list.put("grand_total", (contestant_list.get("upper_total") + contestant_list.get("lower_total")));
				
		return contestant_list;
	}
	
	// Checks current user score list against possible combinations -> the result is hashmap with possible combinations
	@Override
	public HashMap<String, Integer> returnDeltaCombinations(HashMap<String, Integer> contestant_list, HashMap<String, Integer> possible_combinations, boolean yahtzee_flag) {
		HashMap<String, Integer> delta_combinations = new HashMap<String, Integer>();
		
		// Iterating through both lists for matching
		for (Entry<String, Integer> combo : contestant_list.entrySet()) {
			
			String combo_name = combo.getKey();
			
			for (Entry<String, Integer> possible_combo : possible_combinations.entrySet()) {
				
				String possible_combo_name = possible_combo.getKey();
				
				// Checking if key matches (combination name)
				if (combo_name.compareTo(possible_combo_name) == 0) {
					
					// If combination is not used
					if (combo.getValue() == null) {
						delta_combinations.put(possible_combo.getKey(), possible_combo.getValue());
					} 
					// if combination is yahtzee and it is not equal to null -> change flag (will affect user applying of combo)
					else if (combo_name == "yahtzee" && combo.getValue() != null) {
						yahtzee_flag = true;
					}
					
				}
				
			}
		}
		
		// If no suitable combination found -> allocate those combinations that are not used and put a zero value
		if (delta_combinations.isEmpty()) {
			
			for (Entry<String, Integer> combo : contestant_list.entrySet()) {
								
				// If combination is not used
				if (combo.getValue() == null) {
					delta_combinations.put(combo.getKey(), 0);
				}
				
			}
			
		}
		
		return delta_combinations;
	}
	

	// Functions that will compute possible combinations and put them in classification holder
	// Upper scores
	private void checkOnes(Dice[] dice) {
		
		int local_score = 0;
		
		for(int i = 0; i < dice.length; i++) {
			if (dice[i].getValue() == 1) {
				local_score += dice[i].getValue();
			}
		}
		
		if (local_score > 0) {
			classification_holder.put("one", local_score);
		}
		
	}
	
	private void checkTwos(Dice[] dice) {
		
		int local_score = 0;
		
		for(int i = 0; i < dice.length; i++) {
			if (dice[i].getValue() == 2) {
				local_score += dice[i].getValue();
			}
		}
		
		if (local_score > 0) {
			classification_holder.put("two", local_score);
		}
		
	}
	
	private void checkThrees(Dice[] dice) {
		
		int local_score = 0;
		
		for(int i = 0; i < dice.length; i++) {
			if (dice[i].getValue() == 3) {
				local_score += dice[i].getValue();
			}
		}
		
		if (local_score > 0) {
			classification_holder.put("three", local_score);
		}
		
	}
	
	private void checkFours(Dice[] dice) {
		
		int local_score = 0;
		
		for(int i = 0; i < dice.length; i++) {
			if (dice[i].getValue() == 4) {
				local_score += dice[i].getValue();
			}
		}
		
		if (local_score > 0) {
			classification_holder.put("four", local_score);
		}
		
	}
	
	private void checkFives(Dice[] dice) {
		
		int local_score = 0;
		
		for(int i = 0; i < dice.length; i++) {
			if (dice[i].getValue() == 5) {
				local_score += dice[i].getValue();
			}
		}
		
		if (local_score > 0) {
			classification_holder.put("five", local_score);
		}
		
	}
	
	private void checkSixes(Dice[] dice) {
		
		int local_score = 0;
		
		for(int i = 0; i < dice.length; i++) {
			if (dice[i].getValue() == 6) {
				local_score += dice[i].getValue();
			}
		}
		
		if (local_score > 0) {
			classification_holder.put("six", local_score);
		}
		
	}
	
	// lower combinations 
	
	// TOK - three of kind
	private void checkTOK(Dice[] dice) {
		
		int local_score = 0;
		
		for (Entry<Integer, Integer> row : occurance_holder.entrySet()) {
			
			if (row.getValue() == 3) {
				
				for (Dice d : dice) {
					local_score += d.getValue();
				}
				
				break;
			}
			
		}
		
		if (local_score > 0) {
			classification_holder.put("three_of_kind", local_score);
		}
		
	}
	
	// FOK - four of kind
	private void checkFOK(Dice[] dice) {
		
		int local_score = 0;
		
		for (Entry<Integer, Integer> row : occurance_holder.entrySet()) {
			
			if (row.getValue() == 4) {
				
				for (Dice d : dice) {
					local_score += d.getValue();
				}
				
				break;
			}
			
		}
					
		if (local_score > 0) {
			classification_holder.put("four_of_kind", local_score);
		}
		
	}
	
	// FH - full house
	private void checkFH(Dice[] dice) {
		
		int local_score = 0;
		
		// Flags
		boolean triples = false;
		boolean doubles = false;
		
		// Check for triple combo
		for (Entry<Integer, Integer> row : occurance_holder.entrySet()) {
			
			if (row.getValue() == 3) {
				triples = true;
				break;
			}
			
		}
		
		// Check for doubles combo
		for (Entry<Integer, Integer> row : occurance_holder.entrySet()) {
			
			if (row.getValue() == 2) {
				doubles = true;
				break;
			}
			
		}
		
		if (triples && doubles)	{
			local_score = 25;
		}

		if (local_score > 0) {
			classification_holder.put("full_house", local_score);
		}
		
	}
	
	// SS - small straight
	private void checkSS(Dice[] dice) {
		
		int local_score = 0;
		
		// Since the Dice-array is sorted by DESC-order, it check first three values of array
		if ((dice[0].getValue() == 5) && (dice[1].getValue() == 4) && (dice[2].getValue() == 3) && (dice[3].getValue() == 2) && dice[4].getValue() == 1) {
			
			local_score = 30;
			
		}
					
		if (local_score > 0) {
			classification_holder.put("small_straight", local_score);
		}
		
	}
	
	// LS - large straight
	private void checkLS(Dice[] dice) {
		
		int local_score = 0;
		
		// Since the Dice-array is sorted by DESC-order, it check first three values of array
		if ((dice[0].getValue() == 6) && (dice[1].getValue() == 5) && (dice[2].getValue() == 4) && (dice[3].getValue() == 3) && dice[4].getValue() == 2) {
			
			local_score = 40;
			
		}
					
		if (local_score > 0) {
			classification_holder.put("large_straight", local_score);
		}
		
	}
	
	// Yahtzee
	// If you roll a Yahtzee after having already filled in the Yahtzee space, you get a 100 point bonus.
	private void checkYahtzee(Dice[] dice) {
		
		int local_score = 0;
		
		// Since the Dice-array is sorted by DESC-order, it check first three values of array
		// Hard-coded :)
		if ((dice[0].getValue() == dice[1].getValue()) && (dice[1].getValue() == dice[2].getValue()))
			if ((dice[2].getValue() == dice[3].getValue()) && (dice[3].getValue() == dice[4].getValue()))
				local_score = 50;
					
		if (local_score > 0) {
			classification_holder.put("yahtzee", local_score);
		}
		
	}
	
	// Chance
	private void checkChance(Dice[] dice) {
		
		int local_score = 0;
		
		for (int i = 0; i < dice.length; i++)
			local_score += dice[i].getValue();
		
		if (local_score > 0) {
			classification_holder.put("chance", local_score);
		}
		
	}
	
	
}
