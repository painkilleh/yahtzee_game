package controller;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;

import model.Bot;
import model.Contestant;
import model.Game;
import model.GameFactory;
import model.GameType;
import model.IOXML;
import model.IOXML.extendedData;
import model.IOXML.simpleData;
import model.Player;
import view.MainView;

public class MainController {
	
	private MainView main_view = new MainView();
	private GameFactory GF = new GameFactory();
	private IOXML ioxml = new IOXML();
	protected int choice;
	private List<Contestant> contestants = new ArrayList<Contestant>();
	
	public MainController() {
		
	}
	
	public void execute() {
		choice = main_view.displayMainMenu();
		
		// Main menu logic
		// Start the game
		if (choice == 1) {
			choice = main_view.displayGameModes();
			
			// If Start the game is triggered -> choose mode
			if (choice == 1 || choice == 2) {
				
				int type_of_game = choice;
				
				// Local inits
				int number_of_humans = 0;
				int number_of_bots = 0;
				int max_contestants = 5;
				
					
				choice = main_view.enterHuman();

				while (choice <= 0 || choice > 5) {
					main_view.displayError("The range is between 1 and 5. Choose again...");
					choice = main_view.enterHuman();
				}

				// Saving the number of humans
				number_of_humans = choice;
				int space_left = max_contestants - number_of_humans;

				choice = main_view.enterBOT(space_left);

				while (choice < 0 || choice > 5 || choice + number_of_humans > max_contestants) {
					main_view.displayError("The range of maximum player is reached. Try again..");
					choice = main_view.enterBOT(space_left);
				}

				// Saving the number of bots
				number_of_bots = choice;
				
				// Loop for name appending iter
				for (int i = 0; i < number_of_humans; i++) {
					String name = main_view.enterName(i);

					// Error block - if string is empty
					while (name.isEmpty()) {
						main_view.displayError("Name cannot be empty! Retrying...");
						name = main_view.enterName(i);
					}

					// Appending player to list
					Player player = new Player(name);
					this.contestants.add(player);

				}
				
				// Allocating the end of array to append new bots
				int last_element = this.contestants.size();
				
				for (int j = last_element; j < last_element + number_of_bots; j++) {
					Bot bot = new Bot();
					this.contestants.add(bot);	
				}
				
				// Game init
				if (type_of_game == 1) {
					Game standard_game = GF.getGame(GameType.type.standard, this.contestants);
					standard_game.startTheGame();
				} else if (type_of_game == 2) {
					Game maxi_game = GF.getGame(GameType.type.maxi, this.contestants);
					maxi_game.startTheGame();
				}
				
			// Returning to the main menu	
			} else if (choice == 3) {
				this.execute();
			// Error block
			} else {
				main_view.displayError("Wrong selection of menu! Returning to main menu...");
				this.execute();
			}
		
		// Load the game
		} else if (choice == 2) {
			
			choice = main_view.chooseGameVerboseMode();
			String game_file_path = null;
			String game_status = null;
			
			while (choice <= 0 || choice > 3) {
				main_view.displayError("Wrong selection of menu! Retrying...");
				choice = main_view.chooseGameVerboseMode();
			}
			
			if (choice == 1) {
				
				Map<String, simpleData[]> listing = ioxml.verboseSimple();
				choice = main_view.chooseGameSV(listing);
				
				while (true) {
					
					if (choice == -1) {
						this.execute();
						break;
					}
					
					if (choice > listing.size() || choice < 0) {
						main_view.displayError("Wrong selection of index! Retrying...");
						choice = main_view.chooseGameSV(listing);
					} else {
						
						
						int i = 0;
						
						for (Entry<String, simpleData[]> row : listing.entrySet()) {
							
							if (choice == i) {
								game_file_path = row.getKey();
								game_status = row.getValue()[i].status;
								break;
							}
							
							i++;
						}
						
						if (game_status.compareTo("unfinished") == 0) {
							ioxml.loadTheGame(game_file_path);
							break;
						} else {
							main_view.displayError("The game is finished. Returning to main menu...");
							this.execute();
							break;
						}
						
					}
					
					
					
				}
				
			} else if (choice == 2) {
				
				Map<String, extendedData[]> listing = ioxml.verboseExtended();
				choice = main_view.chooseGameEV(listing);
				
				while (true) {
					
					if (choice == -1) {
						this.execute();
						break;
					}
					
					if (choice > listing.size() || choice < 0) {
						main_view.displayError("Wrong selection of index! Retrying...");
						choice = main_view.chooseGameEV(listing);
					} else {
						
						int i = 0;
						
						for (Entry<String, extendedData[]> row : listing.entrySet()) {
							
							if (choice == i) {
								game_file_path = row.getKey();
								game_status = row.getValue()[i].status;
								break;
							}
							
							i++;
						}
						
						if (game_status.compareTo("unfinished") == 0) {
							ioxml.loadTheGame(game_file_path);
							break;
						} else {
							main_view.displayError("The game is finished. Returning to main menu...");
							this.execute();
							break;
						}
					}
					
				}
				
			} else if (choice == 3) {
				this.execute();
			}
			
			
			
		// Exit the application
		} else if (choice == 3) {
			System.exit(-99);
			
		// Error block
		} else {
			main_view.displayError("Wrong selection of menu! Returning to main menu...");
			this.execute();
		}
		
	}

}
