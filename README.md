# Introduction
This project is devoted to the implementation of Yahtzee game that follows MVC-architecture and implements code, based on three basic UML patterns (Strategy, Observer, Factory). The game has two types of game modes : standard game and maxi yahtzee game. The game progress can be saved and loaded by user's desire...

## Class diagram
The repo contains the UML-class diagram that reflects the architecture of the software, but only relations and dependencies. They can be found in root-folder.

### Execution
To make the code work, download the code and execute program/Program.class!
